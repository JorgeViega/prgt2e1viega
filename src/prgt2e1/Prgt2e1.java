
package prgt2e1;

/**
 * Práctica Obligatoria Tema 2
 *
 * @author Pon aqui tu nombre
 * @date Pon aquí la fecha
 */
public class Prgt2e1 {

    static int euros = 232;
    static int billete200 = 0;
    static int billete20 = 0;
    static int billete10 = 0;
    static int resto = euros;

    public static void main(
            String[] args) {

        // Hacer calculos
        resto -= 200;
        billete200++;
        resto -= 20;
        billete20++;
        resto -= 10;
        billete10++;
         // Mostrar el resultado
        System.out.println("El minimo nº de billetes para "+euros+"€ es: "+ billete200 +" billetes de 200 , "+ billete20 +" billetes de 20 , "+billete10+" billetes de 10 y sobran "+resto+"€.");
    }
}
